<?php

module_load_include('php', 'drupal_api_facade', 'SystemApi');
module_load_include('php', 'drupal_api_facade', 'DrupalCommonApi');

class Drupal7Api extends DrupalCommonApi implements SystemApi {

  function __construct($callerModuleDir) {
    $this->callerModuleDir = $callerModuleDir;
  }

  public function addCSS($pathInModuleDir, $module = NULL, $media = 'all') {
    drupal_add_css(drupal_get_path('module', is_null($module) ? $this->callerModuleDir : $module) . '/' . $pathInModuleDir, array('media' => $media));
  }

  public function redirect($path = '', $httpResponseCode = 302) {
    drupal_goto($path, array(), $httpResponseCode);
  }

  public function isNodeOfType($type) {
    if (arg(0) != 'node' || !is_numeric(arg(1)))
      return FALSE;
    $nid = arg(1);
    $node = node_load($nid);
    return ($node->type == $type);
  }

  public function getCurrentPath() {
    return current_path();
  }

  public function execHttpRequest($url, array $options = array()) {
    return drupal_http_request($url, $options);
  }

  public function invokeRulesEvent($eventName, $args = NULL) {
    if (!$this->isModuleEnabled('rules')) {
      throw new Exception("Error invoking Rules event '$eventName': Rules not installed!");
    }
    ($args == NULL) ? rules_invoke_event_by_args($eventName) : rules_invoke_event_by_args($eventName, $args);
  }

  /** @inheritdoc */
  public function getFieldInfoByName($fieldName) {
    return field_info_field($fieldName);
  }

  /** @inheritdoc */
  public function getFieldInfoById($fieldId) {
    return field_info_field_by_id($fieldId);
  }

  /** @inheritdoc */
  public function getListFieldAllowedValues($field, $instance = NULL, $entity_type = NULL, $entity = NULL) {
    return list_allowed_values($field, $instance, $entity_type, $entity);
  }
}
