<?php

module_load_include('php', 'drupal_api_facade', 'SystemApi');
module_load_include('php', 'drupal_api_facade', 'DrupalCommonApi');

class Drupal6Api extends DrupalCommonApi implements SystemApi {

  function __construct($callerModuleDir) {
    $this->callerModuleDir = $callerModuleDir;
  }

  public function addCSS($pathInModuleDir, $module = NULL, $media = 'all') {
    drupal_add_css(drupal_get_path('module', is_null($module) ? $this->callerModuleDir : $module) . '/' . $pathInModuleDir, 'module', $media, TRUE);
  }

  public function redirect($path = '', $httpResponseCode = 302) {
    drupal_goto($path, NULL, NULL, $httpResponseCode);
  }

  public function isNodeOfType($type) {
    if (arg(0) != 'node' || !is_numeric(arg(1)))
      return FALSE;
    $nid = arg(1);
    $node = node_load(array('nid' => $nid));
    return ($node->type == $type);
  }

  public function execHttpRequest($url, array $options = array()) {
    $headers = array_key_exists('headers', $options) ? $options['headers'] : array();
    $method = array_key_exists('method', $options) ? $options['method'] : 'GET';
    $data = array_key_exists('data', $options) ? $options['data'] : NULL;
    $timeout = array_key_exists('timeout', $options) ? $options['timeout'] : 30.0;
    return drupal_http_request($url, $headers, $method, $data, 3, $timeout);
  }


  public function invokeRulesEvent($eventName, $args = NULL) {
    if (!$this->isModuleEnabled('rules')) {
      throw new Exception("Error invoking Rules event '$eventName': Rules not installed!");
    }
    ($args == NULL) ? rules_invoke_event($eventName) : rules_invoke_event($eventName, $args);
  }

  public function getFieldInfoByName($fieldName) {
    throw new Exception('Not implemented!');
  }

  public function getFieldInfoById($fieldId) {
    throw new Exception('Not implemented!');
  }

  /** @inheritdoc */
  public function getListFieldAllowedValues($field, $instance = NULL, $entity_type = NULL, $entity = NULL) {
    throw new Exception('Not implemented!');
  }
}
