<?php

interface SystemApi {

  const LANGUAGE_NONE = 'und';

  public function logDebug($message, $module = NULL, $variables = array());
  public function logNotice($message, $module = NULL, $variables = array());
  public function logError($message, $module = NULL, $variables = array());

  public function isModuleEnabled($moduleName);

  public function addJSInline($jsCode);
  public function addJSFromFile($pathInModuleDir, $module = NULL);

  /**
   * Adds a setting to Drupal's global storage of JavaScript settings.
   * Per-page settings are required by some modules to function properly.
   * The settings will be accessible at Drupal.settings.
   * @param string $settingsKey
   * @param array $settings
   * @return mixed
   */
  public function addJSSettings($settingsKey, array $settings);

  /**
   * Adds a CSS file to the stylesheet queue.
   * @param string $pathInModuleDir The path to the CSS file relative to the module dir.
   * @param string $module Module name.
   * @param string $media (optional) The media type for the stylesheet, e.g., all, print, screen.
   * @return mixed
   */
  public function addCSS($pathInModuleDir, $module = NULL, $media = 'all');

  public function getVariable($name, $default);
  public function setVariable($name, $value);

  /**
   * @param string $string the permission, such as "administer nodes", being checked for.
   * @param object $account (optional) the account to check, if not given use currently logged in user.
   * @return bool TRUE if the current user has the requested permission.
   */
  public function isUserGivenPermission($string, $account = NULL);

  public function t($string, $args = array());
  public function mail($module, $key, $to, $language, $params = array(), $from = NULL, $send = TRUE);

  /**
   * Returns the default language, as an object, or one of its properties.
   *
   * @param $property
   *   (optional) The property of the language object to return.
   *
   * @return
   *   Either the language object for the default language used on the site,
   *   or the property of that object named in the $property parameter.
   */
  public function getDefaultLanguage($property = NULL);

  /**
   * Redirect user to a different page.
   *   Valid HTTP response codes as per RFC 2616 section 10.3 are:
   *   - 301 Moved Permanently (the recommended value for most redirects)
   *   - 302 Found (default in Drupal and PHP, sometimes used for spamming search
   *         engines)
   *   - 303 See Other
   *   - 304 Not Modified
   *   - 305 Use Proxy
   *   - 307 Temporary Redirect (alternative to "503 Site Down for Maintenance")
   *   Note: Other values are defined by RFC 2616, but are rarely used and poorly
   *   supported.
   * @param string $path Drupal path or a full URL.
   * @param int $httpResponseCode
   */
  public function redirect($path = '', $httpResponseCode = 302);
  
  public function getCurNid();
  public function isNodeOfType($type);
  public function isEditingNode();
  public function getCurPageTitle();
  public function isFrontPage();

  /**
   * Return the current URL path of the page being viewed.
   * For nodes returns node/n rather then path alias.
   * @return string the current Drupal URL path.
   */
  public function getCurrentPath();

  public function getUrlForLocalPath($localPath);

  /**
   * Get absolute URL for current page.
   * @return mixed
   */
  public function getUrlForCurrentPage();

  /**
   * Given a path alias, return the internal path it represents.
   *
   * @param $path
   *   A Drupal path alias.
   * @param $path_language
   *   An optional language code to look up the path in.
   *
   * @return
   *   The internal path represented by the alias, or the original alias if no
   *   internal path was found.
   */
  public function getInternalPath($path, $path_language = NULL);

  /**
   * Check if current page path is one of the paths in the given list.
   * Front page should be specified as '&lt;front&gt;' in the list.
   *
   * @param string $pagePaths comma-separated list of page paths
   * @return bool
   */
  public function isCurrentPageInPathsList($pagePaths);

  /**
   * Given an internal Drupal path, return the alias set by the administrator.
   *
   * If no path is provided, the function will return the alias of the current
   * page.
   *
   * @param $path
   *   An internal Drupal path.
   * @param $path_language
   *   An optional language code to look up the path in.
   *
   * @return
   *   An aliased path if one was found, or the original path if no alias was
   *   found.
   */
  public function getPathAlias($path = NULL, $path_language = NULL);

  /**
   * Filters HTML to prevent cross-site-scripting (XSS) vulnerabilities:
   * - Removes characters and constructs that can trick browsers.
   * - Makes sure all HTML entities are well-formed.
   * - Makes sure all HTML tags and attributes are well-formed.
   * - Makes sure no HTML tags contain URLs with a disallowed protocol (e.g.
   *   javascript:).
   *
   * @param string $string
   *   The string with raw HTML in it. It will be stripped of everything that can
   *   cause an XSS attack.
   * @return mixed
   *   An XSS safe version of $string, or an empty string if $string is not
   *   valid UTF-8.
   */
  public function filterXSS($string);

  public function getCurUserId();
  public function getCurUserName();

  /**
   * Determine whether the user has a given privilege.
   *
   * @param $permissionString
   *   The permission, such as "administer nodes", being checked for.
   * @param $account
   *   (optional) The account to check, if not given use currently logged in user.
   *
   * @return
   *   Boolean TRUE if the user has the requested permission.
   *
   * All permission checks in Drupal should go through this function. This
   * way, we guarantee consistent behavior, and ensure that the superuser
   * can perform all actions.
   */
  function isUserHasPermission($permissionString, $account = NULL);

  /**
   * Performs an HTTP request.
   *
   * An HTTP client implementation. Handles any HTTP requests. Handles redirects.
   *
   * @param string $url
   *   A string containing a fully qualified URI.
   * @param array $options
   *   (optional) An array that can have one or more of the following elements:
   *   - headers: An array containing request headers to send as name/value pairs.
   *   - method: A string containing the request method. Defaults to 'GET'.
   *   - data: A string containing the request body, formatted as
   *     'param=value&param=value&...'; to generate this, use http_build_query().
   *     Defaults to NULL.
   *   - max_redirects: An integer representing how many times a redirect
   *     may be followed. Defaults to 3.
   *   - timeout: A float representing the maximum number of seconds the function
   *     call may take. The default is 30 seconds. If a timeout occurs, the error
   *     code is set to the HTTP_REQUEST_TIMEOUT constant.
   *   - context: A context resource created with stream_context_create().
   *
   * @return object
   *   An object that can have one or more of the following components:
   *   - request: A string containing the request body that was sent.
   *   - code: An integer containing the response status code, or the error code
   *     if an error occurred.
   *   - protocol: The response protocol (e.g. HTTP/1.1 or HTTP/1.0).
   *   - status_message: The status message from the response, if a response was
   *     received.
   *   - redirect_code: If redirected, an integer containing the initial response
   *     status code.
   *   - redirect_url: If redirected, a string containing the URL of the redirect
   *     target.
   *   - error: If an error occurred, the error message. Otherwise not set.
   *   - headers: An array containing the response headers as name/value pairs.
   *     HTTP header names are case-insensitive (RFC 2616, section 4.2), so for
   *     easy access the array keys are returned in lower case.
   *   - data: A string containing the response body that was received.
   */
  public function execHttpRequest($url, array $options = array());

  /**
   * Returns data about an individual field, given a field name.
   *
   * @param $fieldName
   *   The name of the field to retrieve. $field_name can only refer to a
   *   non-deleted, active field. For deleted fields, use
   *   field_info_field_by_id(). To retrieve information about inactive fields,
   *   use field_read_fields().
   *
   * @return
   *   The field array, as returned by field_read_fields(), with an
   *   additional element 'bundles', whose value is an array of all the bundles
   *   this field belongs to keyed by entity type. NULL if the field was not
   *   found.
   *
   * @see getFieldInfoById()
   */
  public function getFieldInfoByName($fieldName);

  /**
   * Returns data about an individual field, given a field ID.
   *
   * @param $fieldId
   *   The id of the field to retrieve. $field_id can refer to a
   *   deleted field, but not an inactive one.
   *
   * @return
   *   The field array, as returned by field_read_fields(), with an
   *   additional element 'bundles', whose value is an array of all the bundles
   *   this field belongs to.
   *
   * @see getFieldInfoByName()
   */
  public function getFieldInfoById($fieldId);

  /**
   * Returns the array of allowed values for a list field.
   *
   * The strings are not safe for output. Keys and values of the array should be
   * sanitized through field_filter_xss() before being displayed.
   *
   * @param $field
   *   The field definition.
   * @param $instance
   *   (optional) A field instance array. Defaults to NULL.
   * @param $entity_type
   *   (optional) The type of entity; e.g. 'node' or 'user'. Defaults to NULL.
   * @param $entity
   *   (optional) The entity object. Defaults to NULL.
   *
   * @return
   *   The array of allowed values. Keys of the array are the raw stored values
   *   (number or text), values of the array are the display labels.
   */
  public function getListFieldAllowedValues($field, $instance = NULL, $entity_type = NULL, $entity = NULL);

  public function invokeRulesEvent($eventName, $args = NULL);
}
