<?php

module_load_include('php', 'drupal_api_facade', 'SystemApi');

abstract class DrupalCommonApi implements SystemApi {

  protected $callerModuleDir;

  public function isModuleEnabled($moduleName) {
    return module_exists($moduleName);
  }

  public function addJSFromFile($pathInModuleDir, $module = NULL) {
    drupal_add_js(drupal_get_path('module', is_null($module) ? $this->callerModuleDir : $module) . '/' . $pathInModuleDir);
  }

  public function addJSInline($jsCode) {
    drupal_add_js($jsCode, 'inline');
  }

  public function addJSSettings($settingsKey, array $settings) {
    drupal_add_js(array($settingsKey => $settings), 'setting');
  }

  public function getCurNid() {
    if (arg(0) != 'node' || !is_numeric(arg(1)))
      throw new Exception("Can't be used in this context.");
    return arg(1);
  }

  public function getCurUserId() {
    global $user;
    return $user->uid;
  }

  public function getCurUserName() {
    global $user;
    return $user->name;
  }

  /** @inheritdoc */
  function isUserHasPermission($permissionString, $account = NULL) {
    return user_access($permissionString, $account);
  }

  public function getDefaultLanguage($property = NULL) {
    return language_default($property);
  }

  public function getUrlForLocalPath($localPath) {
    $basePath = $_SERVER["SERVER_NAME"] . base_path();
    $prefix = isset($_SERVER['HTTPS']) ? "https://" : "http://";
    return "$prefix$basePath$localPath";
  }

  public function getUrlForCurrentPage() {
    return url(current_path(), array('absolute' => TRUE));
  }

  public function getInternalPath($path, $path_language = NULL) {
    return drupal_get_normal_path($path, $path_language);
  }

  public function isCurrentPageInPathsList($pagePaths) {
    $pagePaths = trim($pagePaths);
    $paths = array();
    if (strlen($pagePaths) > 0) {
      $paths = explode(',', $pagePaths);
    }
    $isPageToProcess = false;
    foreach ($paths as $path) {
      $path = trim($path);
      if ($path == '<front>' && $this->isFrontPage()) {
        $isPageToProcess = true;
        break;
      }
      elseif ($this->getInternalPath($path) == $this->getInternalPath($this->getCurrentPath())) {
        $isPageToProcess = true;
        break;
      }
    }
    return $isPageToProcess;
  }

  public function getPathAlias($path = NULL, $path_language = NULL) {
    return drupal_get_path_alias($path, $path_language);
  }

  public function getVariable($name, $default) {
    return variable_get($name, $default);
  }

  public function setVariable($name, $value) {
    variable_set($name, $value);
  }

  public function isEditingNode() {
    return arg(2) == "edit";
  }

  public function getCurPageTitle() {
    return drupal_get_title();
  }

  public function isFrontPage() {
    return drupal_is_front_page();
  }

  public function getCurrentPath() {
    return $_GET['q'];
  }

  public function isUserGivenPermission($string, $account = NULL) {
    return user_access($string, $account);
  }

  public function mail($module, $key, $to, $language, $params = array(), $from = NULL, $send = TRUE) {
    return drupal_mail($module, $key, $to, $language, $params, $from, $send);
  }

  public function t($string, $args = array()) {
    return t($string, $args);
  }

  public function filterXSS($string) {
    return filter_xss($string);
  }

  public function logDebug($message, $module = NULL, $variables = array()) {
    watchdog(is_null($module) ? $this->callerModuleDir : $module, $message, $variables, WATCHDOG_DEBUG);
  }

  public function logNotice($message, $module = NULL, $variables = array()) {
    watchdog(is_null($module) ? $this->callerModuleDir : $module, $message, $variables, WATCHDOG_NOTICE);
  }

  public function logError($message, $module = NULL, $variables = array()) {
    watchdog(is_null($module) ? $this->callerModuleDir : $module, $message, $variables, WATCHDOG_ERROR);
  }
}